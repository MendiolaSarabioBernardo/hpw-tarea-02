function ejercicio10(arreglo) {
    var mayor;
    var menor; 
    var suma = 0;
    for(var t in arreglo){
        if (t == 0){
            mayor = arreglo[t];
            menor = arreglo[t];
            suma = suma + arreglo[t];
        }
        if(t > 0){
            suma = suma+arreglo[t];
            if(mayor < arreglo[t]){
                mayor = arreglo[t];
            }
            
            if(menor > arreglo[t]){
                menor = arreglo[t];
            }
        }
    }
    return {
        mayor : mayor,
        menor : menor,
        promedio : suma / arreglo.length
    }
}


